[![pipeline status](https://gitlab.com/yajoman/rpi-cooler/badges/master/pipeline.svg)](https://gitlab.com/yajoman/rpi-cooler/-/commits/master)

# Raspberry Pi Cooler

This repo aims to make my Raspberry Pi cooler. Because it wasn't cool enough.

<!-- prettier-ignore-start -->

<details>
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<summary>Table of contents</summary>

- [Supported OS](#supported-os)
- [Playbooks](#playbooks)
    - [`_rpi_connect`](#_rpi_connect)
    - [`bootstrap`](#bootstrap)
    - [`apply_roles`](#apply_roles)
- [Roles](#roles)
    - [`freedombox`](#freedombox)
    - [`fix_sluggish_mouse`](#fix_sluggish_mouse)
    - [`install_apps`](#install_apps)
    - [`no_pi_user`](#no_pi_user)
    - [`remote_desktop`](#remote_desktop)
    - [`sshd`](#sshd)
- [How to contribute](#how-to-contribute)
- [License](#license)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
</details>

<!-- prettier-ignore-end -->

## Supported OS

- Raspbian 10 (Buster)

## Playbooks

### `_rpi_connect`

This is a dummy role that sets up an inventory manually. It lets you connect to one
Raspberry interactively.

You can use it before any other playbook to start using this collection without even
having declared an inventory!

### `bootstrap`

This playbook can be used to bootstrap a brand new Raspberry Pi in your LAN:

1. Flash latest raspbian on its SD card.
1. Add an empty `ssh` file at the top of its `/boot` partition.
1. Plug your Raspberry Pi to your router by cable.
1. Boot it.
1. Execute from your laptop (in the same LAN):

   ```bash
   ansible-playbook playbooks/_rpi_connect.yml playbooks/bootstrap.yml
   ```

1. Answer the questions.

When you've finished, your Raspberry will have the `pi` user disabled and will have a
new one with the password you provided.

### `apply_roles`

It connects to the Raspberry Pi and applies all roles:

```bash
ansible-playbook --ask-become-pass playbooks/_rpi_connect.yml playbooks/apply_roles.yml
```

## Roles

### `freedombox`

This will install [FreedomBox](https://www.freedombox.org/) in your Raspberry Pi.

Possibly it should work in any other Debian.

It will install NetworkManager and remove anything that conflicts with it.

It will also reboot your Raspberry Pi and tell you the 1st time setup secret key you
will need to continue with your FreedomBox initial setup.

### `fix_sluggish_mouse`

If your mouse is sluggish when using it locally in the Raspberry Pi, just apply this
role.

### `install_apps`

Use this role to install the apps you like quickly.

See [the defaults file](roles/install_apps/defaults/main.yml) to know which variables
you can use.

### `no_pi_user`

This role locks the `pi` user if found in your system.

**⚠ IMPORTANT**: Create another user in the `sudo` group before running this role, or
you will not be able to manage your system anymore. That's why
[the bootstrap playbook](#bootstrap) is provided.

### `remote_desktop`

Installs and enables one `xrdp` server.

### `sshd`

Enables the OpenSSH server and disables password authentication.

## How to contribute

Go check [our contribution guideline](CONTRIUTING.md).

## License

We use [Boost Software License 1.0](COPYING), similar to Apache-2.0, but way shorter to
read.
