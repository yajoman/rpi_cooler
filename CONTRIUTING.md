# Contribution guidelines

<details>
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<summary>Table of contents</summary>

- [How to set up a development environment](#how-to-set-up-a-development-environment)
- [How to contribute](#how-to-contribute)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
</details>

# How to set up a development environment

[VSCode](https://code.visualstudio.com/) is recommended, but use your favorite IDE.

1. Install Python 3.
1. Install [Poetry](https://python-poetry.org/).
1. Execute these commands:

   ```bash
   python3 -m venv .venv
   poetry env use .venv/bin/python
   poetry install
   poetry run pre-commit install
   ```

1. Make sure pre-commit is happy before you send your merge request.

# How to contribute

Well, just as usual in any modern development project.
[Instructions here](https://opensource.guide/how-to-contribute/).
